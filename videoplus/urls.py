from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'videoplus.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','videoplus.views.homepage',name="index"),
    #usuarios
    url(r'^usuarios/' , include('apps.usuarios.urls')),
    #videos
    url(r'^video/' , include('apps.video.urls')),
    #files
    url(r'^files/' , include('apps.upload_file.urls')),
    #categorias
    url(r'^categorias/' , include('apps.categoria.urls')),
    #login
    url(r'^login/$','videoplus.views.login_page',name='login'),
    url(r'^Iogin/$','videoplus.views.login_after_register',name='login2'),
    url(r'^logout/$','videoplus.views.logout_view',name='logout'),
    #playlist
    url(r'^playlist/' , include('apps.playlist.urls')),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

