from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from apps.video.views import listar_videos_paginado_index,listar_videos_paginado_mis_videos,listar_videos_paginado_play_videos



def login_page(request):
    message = None
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username,password=password)
            if user is not None:
                if user.is_active:
                    login(request,user)
                    message = "sucess"
                    return redirect("index")
                else:
                    message = "Tu usario esta inactivo"
            else:
                message = "error"
    else:
        form = LoginForm()
    return render_to_response('usuarios/login.html',{'message':message,'form':form},context_instance=RequestContext(request))

def login_after_register(request):
    message = None
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username,password=password)
            if user is not None:
                if user.is_active:
                    login(request,user)
                    return redirect("index")
                else:
                    message = "Tu usario esta inactivo"
            else:
                message = "error"
    else:
        form = LoginForm()
    return render_to_response('usuarios/login2.html',{'message':message,'form':form},context_instance=RequestContext(request))



def homepage(request):
    return listar_videos_paginado_index(request)

def logout_view(request):
    logout(request)
    return redirect('index')

def mis_videos(request):
    return listar_videos_paginado_mis_videos(request)

def play_video_todos(request):
    return listar_videos_paginado_play_videos(request)


