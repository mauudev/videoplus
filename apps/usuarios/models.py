from django.db import models
from django.contrib.auth.models import User


class Perfiles(models.Model):
    usuario = models.OneToOneField(User,blank=True, null=True, on_delete=models.DO_NOTHING)
    email = models.EmailField()

    def __str__(self):
        return self.usuario.username