from django.conf.urls import patterns, include, url
#from .views import BuscarView
from .views import Registrarse

urlpatterns = patterns('',

        url(r'^registrarse/$' , Registrarse.as_view() , name= "registro_usuario"),
        url(r'^misvideos/$' , 'apps.video.views.listar_videos_paginado_mis_videos' , name= "mis_videos"),



)

