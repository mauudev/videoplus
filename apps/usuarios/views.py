from django.views.generic import FormView
from .forms import UserForm
from django.core.urlresolvers import reverse_lazy
from .models import Perfiles


class Registrarse(FormView):
    template_name = 'usuarios/registrarse.html'
    form_class = UserForm
    success_url = reverse_lazy('login2')

    def form_valid(self, form):#si los datos de registro son validos esto se ejecuta
        user = form.save()
        perfil = Perfiles()
        perfil.usuario = user
        perfil.email = form.cleaned_data['email']
        perfil.save()
        return super(Registrarse, self).form_valid(form)







