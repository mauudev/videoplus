from django import forms
from .models import Video

class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ['categoria','titulo', 'descripcion', 'url']

class EditVideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ['categoria','titulo', 'descripcion']
  # este es un comentario
  #   titulo = forms.CharField(max_length=100)
  #   descripcion = forms.CharField(widget=forms.Textarea)
  # #  categoria = forms.Select()
  #   url = forms.FileField(label='Selecciona un archivo de video')