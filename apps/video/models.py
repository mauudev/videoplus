from django.db import models
from django.contrib.auth.models import User
from apps.categoria.models import Categoria


class Video(models.Model):
    usuario = models.ForeignKey(User,blank=True, null=True, on_delete=models.DO_NOTHING)
    categoria = models.ForeignKey(Categoria,blank=True, null=True, on_delete=models.DO_NOTHING)
    titulo = models.CharField(max_length=100)
    descripcion = models.TextField(max_length=200)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    contador_vista = models.IntegerField()
    url = models.FileField(upload_to='videos/%Y/%m/%d')

    def __str__(self):
        return self.titulo