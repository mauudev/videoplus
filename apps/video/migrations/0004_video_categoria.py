# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('categoria', '0001_initial'),
        ('video', '0003_auto_20141212_0357'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='categoria',
            field=models.ForeignKey(default=1, to='categoria.Categoria'),
            preserve_default=False,
        ),
    ]
