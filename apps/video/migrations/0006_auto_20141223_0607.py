# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('video', '0005_auto_20141223_0605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='categoria',
            field=models.ForeignKey(blank=True, to='categoria.Categoria', on_delete=django.db.models.deletion.DO_NOTHING, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='video',
            name='usuario',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.DO_NOTHING, null=True),
            preserve_default=True,
        ),
    ]
