from django.shortcuts import render_to_response, redirect, render
from .forms import VideoForm, EditVideoForm
from django.http import HttpResponseNotFound
from .models import Video
from django.template import RequestContext
from django.utils import timezone
from apps.categoria.models import Categoria
from apps.playlist.models import Playlist
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import TemplateView
from apps.playlist.views import verify_playlist, eliminar_video_playlist

class BuscarView(TemplateView):
    template_name = 'video/buscar.html'

    def post(self,request,*args,**kwargs):
        buscar = request.POST['buscar']
        files = Video.objects.filter(titulo__icontains = buscar)
        categorias = Categoria.objects.all()
        if files:
            # video_list = Video.objects.all()
            return render_to_response('video/buscar.html', {'search':True,'files': files,'categorias':categorias,'flag':True},context_instance=RequestContext(request))
        else:
            return render_to_response('video/buscar.html', {'search':False,'categorias':categorias,'flag':False},context_instance=RequestContext(request))


@login_required
def subir_video(request):
    user = request.user
    categorias = Categoria.objects.all()
    files = Video.objects.filter(usuario_id=user.id)
    if request.method == 'POST':
        form = VideoForm(request.POST, request.FILES)
        if form.is_valid():
            cat = form.cleaned_data['categoria']
            nuevo_video = Video(usuario=user, categoria=cat, titulo=request.POST['titulo'],
                                descripcion=request.POST['descripcion'],
                                fecha_creacion=timezone.now().date(), contador_vista=0, url=request.FILES['url'])
            nuevo_video.save(form)
            #return redirect('mis_videos')
            # return render_to_response('usuarios/mis_videos.html', {'categorias':categorias,'files':files,'flag':True}, context_instance=RequestContext(request))
            return listar_videos_paginado_mis_videos(request)
    else:
        form = VideoForm()
    #tambien se puede utilizar render_to_response
    #return render_to_response('upload_video.html', {'form': form}, context_instance = RequestContext(request))
    return render_to_response('video/upload_video.html', {'form': form,'categorias':categorias,'files':files,'flag':True}, context_instance=RequestContext(request))


@login_required()
def listar_videos(request):
    user_id = request.user.id
    try:
        files = Video.objects.filter(usuario_id=user_id)
        categorias = Categoria.objects.all()
        if files:
            return render_to_response("usuarios/mis_videos.html", {'files': files,'categorias':categorias,'flag': True},
                                      context_instance=RequestContext(request))
        else:
            message = "No tienes videos en la nube, haz clic en Subir videos para comenzar :)"
            return render(request, 'usuarios/mis_videos.html', {'message': message,'categorias':categorias, 'flag': False})
    except:
        return HttpResponseNotFound('<h1>Oops! ocurrio un problema al listar videos :(</h1>')


@login_required()
def editar_video(request, video_id):
    video = get_object_or_404(Video, pk=video_id)
    categorias = Categoria.objects.all()
    if request.method == 'POST':
        form = EditVideoForm(request.POST, instance=video)
        if form.is_valid():
            form.save()
            return listar_videos_paginado_mis_videos(request)
    else:
        form = EditVideoForm(instance=video)
    return render_to_response('video/edit_video.html', {'form': form,'categorias':categorias}, context_instance=RequestContext(request))

#NOTA: flag:True es para asegurarnos que estamos enviado objetos no nulos de la consulta sql a la template

@login_required()
def eliminar_video(request, video_id):
    try:
        message = verify_playlist(request,video_id)
        user_id = request.user.id
        if message == 'existe':
            vd = Playlist.objects.filter(video_id=video_id).filter(usuario_id=user_id)
            vd.delete()
            video_target = get_object_or_404(Video, pk=video_id)
            video_target.delete()
            return listar_videos_paginado_mis_videos(request)
    except:
        return HttpResponseNotFound('<h1>Ocurrio un error al tratar de eliminar el video :(</h1>')


def listar_todos_los_videos(request):
    files = Video.objects.all()
    categorias = Categoria.objects.all()
    if files:
        return render_to_response('indesx.html', {'files': files,'categorias':categorias,'flag':True},context_instance=RequestContext(request))
    else:
        return render_to_response('indesx.html', {'flag':False},context_instance=RequestContext(request))


def listar_videos_paginado_index(request):
    video_list = Video.objects.all().order_by('-fecha_creacion')
    categorias = Categoria.objects.all()
    mas_vistos = Video.objects.all().order_by('-contador_vista')[:3]
    if video_list:
        paginator = Paginator(video_list, 9) # Muestra 10 videos por pagina
        page = request.GET.get('page')
        try:
             files = paginator.page(page)
        except PageNotAnInteger:
             #Si la pagina no es un entero, redirige a la primera pagina
             files = paginator.page(1)
        except EmptyPage:
             #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
             files = paginator.page(paginator.num_pages)
        return render_to_response('index.html', {'mas_vistos':mas_vistos,'files': files,'categorias':categorias,'flag':True},context_instance=RequestContext(request))
    else:
        return render_to_response('index.html', {'mas_vistos':mas_vistos,'categorias':categorias,'flag':False},context_instance=RequestContext(request))


@login_required()
def listar_videos_paginado_mis_videos(request):
    user_id = request.user.id
    video_list = Video.objects.filter(usuario_id=user_id).order_by('-fecha_creacion')
    categorias = Categoria.objects.all()
    if video_list:
        paginator = Paginator(video_list, 9) # Muestra 10 videos por pagina
        page = request.GET.get('page')
        try:
             files = paginator.page(page)
        except PageNotAnInteger:
             #Si la pagina no es un entero, redirige a la primera pagina
             files = paginator.page(1)
        except EmptyPage:
             #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
             files = paginator.page(paginator.num_pages)
        return render_to_response('usuarios/mis_videos.html', {'files': files,'categorias':categorias,'flag':True},context_instance=RequestContext(request))
    else:
        return render_to_response('usuarios/mis_videos.html', {'categorias':categorias,'flag':False},context_instance=RequestContext(request))


def listar_videos_paginado_play_videos(request):
    user_id = request.user.id
    video_list = Video.objects.all().order_by('-fecha_creacion')
    categorias = Categoria.objects.all()
    if video_list:
        paginator = Paginator(video_list, 9) # Muestra 10 videos por pagina
        page = request.GET.get('page')
        try:
             files = paginator.page(page)
        except PageNotAnInteger:
             #Si la pagina no es un entero, redirige a la primera pagina
             files = paginator.page(1)
        except EmptyPage:
             #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
             files = paginator.page(paginator.num_pages)
        return render_to_response('video/play_videos.html', {'files': files,'categorias':categorias,'flag':True},context_instance=RequestContext(request))
    else:
        return render_to_response('video/play_videos.html', {'categorias':categorias,'flag':False},context_instance=RequestContext(request))



def play_video(request, video_id):
        video_list = Video.objects.all().order_by('-fecha_creacion')
        categorias = Categoria.objects.all()
        video_target = get_object_or_404(Video, pk=video_id)
        video_target.contador_vista = video_target.contador_vista + 1
        video_target.save()
        video = Video.objects.filter(pk=video_id)
        if video_list:
             paginator = Paginator(video_list, 9) # Muestra 10 videos por pagina
             page = request.GET.get('page')
             try:
                  files = paginator.page(page)
             except PageNotAnInteger:
                  #Si la pagina no es un entero, redirige a la primera pagina
                  files = paginator.page(1)
             except EmptyPage:
                  #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
                  files = paginator.page(paginator.num_pages)
             return render_to_response('video/play_video.html', {'files': files,'categorias':categorias,'video':video,'flag':True},context_instance=RequestContext(request))
        else:
             return render_to_response('video/play_video.html', {'categorias':categorias,'video':video,'flag':False},context_instance=RequestContext(request))



def play_video_pl(request, video_id):
        video_list = Video.objects.all().order_by('-fecha_creacion')
        user_id = request.user.id
        categorias = Categoria.objects.all()
        video_target = get_object_or_404(Video, pk=video_id)
        video_target.contador_vista = video_target.contador_vista + 1
        video_target.save()
        video = Video.objects.filter(pk=video_id)
        #message = agregar_playlist(request,video_id)#existe o no existe
        playlist = Playlist.objects.filter(usuario_id=user_id).values('video__id','video__titulo','usuario__id')
        playlist_flag = False
        if playlist:
             playlist_flag = True

        if video_list:
             paginator = Paginator(video_list, 9) # Muestra 10 videos por pagina
             page = request.GET.get('page')
             try:
                  files = paginator.page(page)
             except PageNotAnInteger:
                  #Si la pagina no es un entero, redirige a la primera pagina
                  files = paginator.page(1)
             except EmptyPage:
                  #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
                  files = paginator.page(paginator.num_pages)
             return render_to_response('video/play_video.html', {'playlist':playlist,'playlist_flag':playlist_flag,
                                                                 'files': files,'categorias':categorias,'video':video,'flag':True},
                                                                 context_instance=RequestContext(request))
        else:
             return render_to_response('video/play_video.html', {'playlist':playlist,'playlist_flag':playlist_flag,
                                                                 'categorias':categorias,'video':video,'flag':False}
                                                                 ,context_instance=RequestContext(request))


