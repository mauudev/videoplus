from django.conf.urls import patterns, include, url
from .views import BuscarView

urlpatterns = patterns('',

 url(r'^subir/$', 'apps.video.views.subir_video', name="upload"),
 url(r'^editar/(?P<video_id>\d+)/$','apps.video.views.editar_video', name="editar"),
 url(r'^eliminar/(?P<video_id>\d+)/$','apps.video.views.eliminar_video', name='eliminar'),
 url(r'^play/(?P<video_id>\d+)/$','apps.video.views.play_video_pl', name='reproducir2'),
 url(r'^playlist/(?P<video_id>\d+)/$','apps.video.views.play_video', name='reproducir1'),
 url(r'^buscar/$',BuscarView.as_view(), name='buscar_video'),


 )