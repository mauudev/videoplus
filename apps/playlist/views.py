from django.shortcuts import render_to_response, redirect, render
from django.http import HttpResponseNotFound
from .models import Playlist
from django.template import RequestContext
from apps.categoria.models import Categoria
from apps.video.models import Video
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

@login_required()
def listar_playlist(request):
    user_id = request.user.id
    try:
        files = Video.objects.filter(usuario_id=user_id)
        categorias = Categoria.objects.all()
        if files:
            return render_to_response("usuarios/mis_videos.html", {'files': files,'categorias':categorias,'flag': True},
                                      context_instance=RequestContext(request))
        else:
            message = "No tienes videos en la nube, haz clic en Subir videos para comenzar :)"
            return render(request, 'usuarios/mis_videos.html', {'message': message,'categorias':categorias, 'flag': False})
    except:
        return HttpResponseNotFound('<h1>Oops! ocurrio un problema al listar videos :(</h1>')





def verify_playlist(request,video_id):
    user_id = request.user.id
    videos_pl = Playlist.objects.filter(video_id=video_id).filter(usuario_id=user_id)
    message = 'no-existe'
    if videos_pl:
        message = 'existe'
        return message
    return message

@login_required()
def agregar_playlist(request,video_id):
        user_id = request.user.id
        message = verify_playlist(request,video_id)
        if message == 'no-existe':
            vd = Playlist(usuario_id = user_id, video_id = video_id)
            vd.save()
        video_list = Video.objects.all()
        user_id = request.user.id
        categorias = Categoria.objects.all()
        video_target = get_object_or_404(Video, pk=video_id)
        video_target.contador_vista = video_target.contador_vista + 1
        video_target.save()
        video = Video.objects.filter(pk=video_id)
        playlist = Playlist.objects.filter(usuario_id=user_id).values('video__id','video__titulo','usuario__id')
        playlist_flag = False
        if playlist:
             playlist_flag = True
        if video_list:
             paginator = Paginator(video_list, 6) # Muestra 10 videos por pagina
             page = request.GET.get('page')
             try:
                  files = paginator.page(page)
             except PageNotAnInteger:
                  #Si la pagina no es un entero, redirige a la primera pagina
                  files = paginator.page(1)
             except EmptyPage:
                  #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
                  files = paginator.page(paginator.num_pages)
             return render_to_response('video/play_video.html', {'message':message,'playlist':playlist,'playlist_flag':playlist_flag,
                                                                 'files': files,'categorias':categorias,'video':video,'flag':True},
                                                                 context_instance=RequestContext(request))
        else:
             return render_to_response('video/play_video.html', {'message':message,'playlist':playlist,'playlist_flag':playlist_flag,
                                                                 'categorias':categorias,'video':video,'flag':False}
                                                                 ,context_instance=RequestContext(request))



@login_required()
def eliminar_video_playlist(request,video_id):
        message = verify_playlist(request,video_id)
        user_id = request.user.id
        if message == 'existe':
            vd = Playlist.objects.filter(video_id=video_id).filter(usuario_id=user_id)
            vd.delete()
        video_list = Video.objects.all()
        categorias = Categoria.objects.all()
        video_target = get_object_or_404(Video, pk=video_id)
        video_target.contador_vista = video_target.contador_vista + 1
        video_target.save()
        video = Video.objects.filter(pk=video_id)
        playlist = Playlist.objects.filter(usuario_id=user_id).values('video__id','video__titulo','usuario__id')
        playlist_flag = False
        if playlist:
             playlist_flag = True
        if video_list:
             paginator = Paginator(video_list, 6) # Muestra 10 videos por pagina
             page = request.GET.get('page')
             try:
                  files = paginator.page(page)
             except PageNotAnInteger:
                  #Si la pagina no es un entero, redirige a la primera pagina
                  files = paginator.page(1)
             except EmptyPage:
                  #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
                  files = paginator.page(paginator.num_pages)
             return render_to_response('video/play_video.html', {'message':message,'playlist':playlist,'playlist_flag':playlist_flag,
                                                                 'files': files,'categorias':categorias,'video':video,'flag':True},
                                                                 context_instance=RequestContext(request))
        else:
             return render_to_response('video/play_video.html', {'message':message,'playlist':playlist,'playlist_flag':playlist_flag,
                                                                 'categorias':categorias,'video':video,'flag':False}
                                                                 ,context_instance=RequestContext(request))