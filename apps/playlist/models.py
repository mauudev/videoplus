from django.db import models
from django.contrib.auth.models import User
from apps.video.models import Video

class Playlist(models.Model):
    usuario = models.ForeignKey(User,blank=True, null=True, on_delete=models.DO_NOTHING)
    video = models.ForeignKey(Video, blank=True, null=True, on_delete=models.DO_NOTHING)

