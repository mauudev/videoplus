from django.conf.urls import patterns, include, url
#from .views import BuscarView

urlpatterns = patterns('',

   # url(r'^buscar/' , BuscarView.as_view(), name='buscar')
    url(r'^add/(?P<video_id>\d+)/$','apps.playlist.views.agregar_playlist', name="add_playlist"),
    url(r'^del/(?P<video_id>\d+)/$','apps.playlist.views.eliminar_video_playlist', name='del_playlist'),
)
