# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('video', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Playlist',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('usuario_id', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('video_id', models.ForeignKey(to='video.Video')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
