# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('playlist', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='playlist',
            old_name='usuario_id',
            new_name='usuario',
        ),
        migrations.RenameField(
            model_name='playlist',
            old_name='video_id',
            new_name='video',
        ),
    ]
