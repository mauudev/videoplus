# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('playlist', '0002_auto_20141211_0309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playlist',
            name='usuario',
            field=models.ForeignKey(null=True, blank=True, to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.DO_NOTHING),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playlist',
            name='video',
            field=models.ForeignKey(null=True, blank=True, to='video.Video', on_delete=django.db.models.deletion.DO_NOTHING),
            preserve_default=True,
        ),
    ]
