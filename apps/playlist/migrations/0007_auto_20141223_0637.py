# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('playlist', '0006_auto_20141223_0625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playlist',
            name='usuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, null=True, blank=True, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playlist',
            name='video',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, null=True, blank=True, to='video.Video'),
            preserve_default=True,
        ),
    ]
