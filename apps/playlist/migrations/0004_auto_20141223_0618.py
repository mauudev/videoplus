# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('playlist', '0003_auto_20141223_0605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playlist',
            name='usuario',
            field=models.ForeignKey(null=True, blank=True, to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.SET_NULL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playlist',
            name='video',
            field=models.ForeignKey(null=True, blank=True, to='video.Video', on_delete=django.db.models.deletion.SET_NULL),
            preserve_default=True,
        ),
    ]
