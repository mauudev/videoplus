from django.shortcuts import render, redirect #puedes importar render_to_response
from .forms import UploadForm
from .models import Document


def upload_file(request):
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(filename = request.POST['filename'],docfile = request.FILES['docfile'])
            newdoc.save(form)
            return redirect("upload")
    else:
        form = UploadForm()
        files = Document.objects.all()
    #tambien se puede utilizar render_to_response
    #return render_to_response('upload_video.html', {'form': form}, context_instance = RequestContext(request))
    return render(request, 'video/upload_video.html', {'form': form})