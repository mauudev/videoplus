from .models import Categoria
from django.shortcuts import render_to_response
from django.template import RequestContext
from apps.video.models import Video
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def listar_videos_por_categoria_paginado(request,categoria_id):
    video_list = Video.objects.filter(categoria_id=categoria_id)
    categorias = Categoria.objects.all()
    mas_vistos = Video.objects.all().order_by('-contador_vista')[:3]
    if video_list:
        paginator = Paginator(video_list, 6) # Muestra 10 videos por pagina
        page = request.GET.get('page')
        try:
             files = paginator.page(page)
        except PageNotAnInteger:
             #Si la pagina no es un entero, redirige a la primera pagina
             files = paginator.page(1)
        except EmptyPage:
             #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
             files = paginator.page(paginator.num_pages)
        return render_to_response('index.html', {'mas_vistos':mas_vistos,'files': files,'categorias':categorias,'flag':True},context_instance=RequestContext(request))
    else:
        return render_to_response('index.html', {'mas_vistos':mas_vistos,'categorias':categorias,'flag':False},context_instance=RequestContext(request))



def listar_todas_las_categorias_paginado(request):
    video_list = Video.objects.all()
    categorias = Categoria.objects.all()
    mas_vistos = Video.objects.all().order_by('-contador_vista')[:3]
    if video_list:
        paginator = Paginator(video_list, 6) # Muestra 10 videos por pagina
        page = request.GET.get('page')
        try:
             files = paginator.page(page)
        except PageNotAnInteger:
             #Si la pagina no es un entero, redirige a la primera pagina
             files = paginator.page(1)
        except EmptyPage:
             #Si la pagina esta fuera del rango (ej. 9999) devuelve la ultima pagina con los resultados
             files = paginator.page(paginator.num_pages)
        return render_to_response('index.html', {'mas_vistos':mas_vistos,'files': files,'categorias':categorias,'flag':True},context_instance=RequestContext(request))
    else:
        return render_to_response('index.html', {'mas_vistos':mas_vistos,'categorias':categorias,'flag':False},context_instance=RequestContext(request))



def listar_por_categorias(request, categoria_id):
    files = Video.objects.filter(categoria_id=categoria_id)
    categorias = Categoria.objects.all()
    mas_vistos = Video.objects.all().order_by('-contador_vista')[:3]
    return render_to_response('index.html',{'mas_vistos':mas_vistos,'files':files,'categorias':categorias,'flag':True},context_instance=RequestContext(request))