from django.conf.urls import patterns, include, url
#from .views import BuscarView

urlpatterns = patterns('',

 url(r'^$', 'apps.categoria.views.listar_todas_las_categorias_paginado', name="categorias"),
 url(r'^(?P<categoria_id>\d+)/$','apps.categoria.views.listar_videos_por_categoria_paginado', name='listar_categoria'),


 )